package com.example.threadpriorityqueue;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView text;

        Pq pq = new Pq();

        pq.Enqueu(5, "calculate");
        pq.Enqueu(4, "Searching");
        pq.Enqueu(1, "Loading");
        pq.Enqueu(3, "Self destructing");

        //loop queue
        new Thread(new Job(pq.Dequeue())).start();
    }
}