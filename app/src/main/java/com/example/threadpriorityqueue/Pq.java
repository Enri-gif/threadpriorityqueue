package com.example.threadpriorityqueue;

public class Pq {
    public class Node{
        private static final int lowestPriority = 999;
        private int priority = lowestPriority;
        private String msg;
        private Node next;
        private int Index;
    }
    private Node headNode = new Node();
    private int nodeCount = 0;

    public void Enqueu(int Priority, String Msg){
        Node current = headNode;
        for(int i = 0; i <= nodeCount; i++){
            if(i == nodeCount){
                Node n = new Node();
                n.msg = Msg;
                n.Index = i;
                n.priority = Priority;
                current.next = n;
            }
            current = current.next;
        }
        nodeCount++;
    }
    public void DeleteAt(int Index){
        Node current = headNode;
        if(Index <= nodeCount){
            for(int i = 0; i <= Index; i++){
                if(Index == i){
                    current.next = current.next.next;
                    nodeCount--;
                }
                current = current.next;
            }
        }
        else throw new IndexOutOfBoundsException();
    }
    public String ItemAt(int Index){
        Node current = headNode;
        if(Index <= nodeCount){
            for(int i = 0; i <= Index; i++){
                if(Index != i)
                    current = current.next;
            }
        }
        else throw new IndexOutOfBoundsException();
        
        return current.msg;
    }
    public String Dequeue(){
        Node current = headNode;
        Node highP = current;
        for(int i = 0; i <= nodeCount; i++){
            if(current.next != null && current.next.priority < highP.priority)
                highP = current.next;
            current = current.next;
        }
        DeleteAt(highP.Index);
        return highP.msg;
    }
}
